
/*config*/
 var scale_object = {
    heat_scale_min:4,
    heat_scale_max:40,
    hum_scale_min:20,
    hum_scale_max:60
 }
    

/*************** */


var set_values_url = "http://localhost:8081/set_values";
var get_values_url = "http://localhost:8081/get_values";
var url = "http://localhost:8081/get_data";

$(document).ready(function()
{
     
    var slider_heat = document.getElementById('slider_temp');
    var slider_hum = document.getElementById('slider_hum');
        noUiSlider.create(slider_heat, {
            start: [20, 80],
            connect:true,
            tooltips: [wNumb({ decimals: 1 }), wNumb({ decimals: 1 }) ],
            range: {
                'min': -10,
                'max': 70
            }    
        });

        noUiSlider.create(slider_hum, {
            start: [20, 80],
            connect:true,
            tooltips: [wNumb({ decimals: 1 }), wNumb({ decimals: 1 }) ],
            range: {
                'min': 0,
                'max': 100
            }    
        });
        slider_heat.noUiSlider.on('set', function(){
            
            scale_object.heat_scale_min = slider_heat.noUiSlider.get()[0];
            scale_object.heat_scale_max = slider_heat.noUiSlider.get()[1];
             $.ajax({
                type: "POST",
                url: set_values_url,
                data: JSON.stringify(scale_object),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data){},
                failure: function(errMsg) {
                    
                }
            });
        });
        
        slider_hum.noUiSlider.on('set', function(){
            
            scale_object.hum_scale_min = slider_hum.noUiSlider.get()[0];
            scale_object.hum_scale_max = slider_hum.noUiSlider.get()[1];
            
             $.ajax({
                type: "POST",
                url: set_values_url,
                data: JSON.stringify(scale_object),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data){},
                failure: function(errMsg) {
                    
                }
            });
        });
      

    $.get(get_values_url,function(json){
       json = JSON.parse(json);
        scale_object.heat_scale_min = json.heat_scale_min;
        scale_object.heat_scale_max = json.heat_scale_max;
        scale_object.hum_scale_min = json.hum_scale_min;
        scale_object.hum_scale_max = json.hum_scale_max;

        slider_hum.noUiSlider.set([scale_object.hum_scale_min,scale_object.hum_scale_max]);
        slider_temp.noUiSlider.set([scale_object.heat_scale_min,scale_object.heat_scale_max]);
       
    });

    $("#not_respond").hide();
    $("#online").hide();

    $("#value_set").hide();
    $("#value_error").hide();

    $("#temperature_map").hide();
    $("#humidity_map").hide();
    $("#temperature_map_2").hide();
    $("#humidity_map_2").hide();
    $("#temperature_map_3").hide();
    $("#humidity_map_3").hide();
    $("#temperature_map_4").hide();
    $("#humidity_map_4").hide();

     $("#temperature_map_s").show();
    $("#humidity_map_s").show();
    $("#temperature_map_2_s").show();
    $("#humidity_map_2_s").show();
    $("#temperature_map_3_s").show();
    $("#humidity_map_3_s").show();
    $("#temperature_map_4_s").show();
    $("#humidity_map_4_s").show();
    $("#config_page").hide();
});

$('li > a').click(function() {
    $('li').removeClass();
    $(this).parent().addClass('active');
});

$("#lvl_1").click(function(){
    $("#temperature_map").show();
    $("#humidity_map").show();
    $("#temperature_map_2").hide();
    $("#humidity_map_2").hide();
    $("#temperature_map_3").hide();
    $("#humidity_map_3").hide();
    $("#temperature_map_4").hide();
    $("#humidity_map_4").hide();

     $("#temperature_map_s").hide();
    $("#humidity_map_s").hide();
    $("#temperature_map_2_s").hide();
    $("#humidity_map_2_s").hide();
    $("#temperature_map_3_s").hide();
    $("#humidity_map_3_s").hide();
    $("#temperature_map_4_s").hide();
    $("#humidity_map_4_s").hide();
    $("#config_page").hide();
});

$("#lvl_2").click(function(){
    $("#temperature_map").hide();
    $("#humidity_map").hide();
    $("#temperature_map_2").show();
    $("#humidity_map_2").show();
    $("#temperature_map_3").hide();
    $("#humidity_map_3").hide();
    $("#temperature_map_4").hide();
    $("#humidity_map_4").hide();

     $("#temperature_map_s").hide();
    $("#humidity_map_s").hide();
    $("#temperature_map_2_s").hide();
    $("#humidity_map_2_s").hide();
    $("#temperature_map_3_s").hide();
    $("#humidity_map_3_s").hide();
    $("#temperature_map_4_s").hide();
    $("#humidity_map_4_s").hide();
    $("#config_page").hide();
});

$("#lvl_3").click(function(){
    $("#temperature_map").hide();
    $("#humidity_map").hide();
    $("#temperature_map_2").hide();
    $("#humidity_map_2").hide();
    $("#temperature_map_3").show();
    $("#humidity_map_3").show();
    $("#temperature_map_4").hide();
    $("#humidity_map_4").hide();

     $("#temperature_map_s").hide();
    $("#humidity_map_s").hide();
    $("#temperature_map_2_s").hide();
    $("#humidity_map_2_s").hide();
    $("#temperature_map_3_s").hide();
    $("#humidity_map_3_s").hide();
    $("#temperature_map_4_s").hide();
    $("#humidity_map_4_s").hide();
    $("#config_page").hide();
});

$("#lvl_4").click(function(){
    $("#temperature_map").hide();
    $("#humidity_map").hide();
    $("#temperature_map_2").hide();
    $("#humidity_map_2").hide();
    $("#temperature_map_3").hide();
    $("#humidity_map_3").hide();
    $("#temperature_map_4").show();
    $("#humidity_map_4").show();

     $("#temperature_map_s").hide();
    $("#humidity_map_s").hide();
    $("#temperature_map_2_s").hide();
    $("#humidity_map_2_s").hide();
    $("#temperature_map_3_s").hide();
    $("#humidity_map_3_s").hide();
    $("#temperature_map_4_s").hide();
    $("#humidity_map_4_s").hide();
    $("#config_page").hide();
});

$("#hum_expanded").click(function(){
    $("#temperature_map").hide();
    $("#humidity_map").hide();
    $("#temperature_map_2").hide();
    $("#humidity_map_2").hide();
    $("#temperature_map_3").hide();
    $("#humidity_map_3").hide();
    $("#temperature_map_4").hide();
    $("#humidity_map_4").hide();

     $("#temperature_map_s").hide();
    $("#humidity_map_s").show();
    $("#temperature_map_2_s").hide();
    $("#humidity_map_2_s").show();
    $("#temperature_map_3_s").hide();
    $("#humidity_map_3_s").show();
    $("#temperature_map_4_s").hide();
    $("#humidity_map_4_s").show();
    $("#config_page").hide();
});

$("#temp_expanded").click(function(){
    $("#temperature_map").hide();
    $("#humidity_map").hide();
    $("#temperature_map_2").hide();
    $("#humidity_map_2").hide();
    $("#temperature_map_3").hide();
    $("#humidity_map_3").hide();
    $("#temperature_map_4").hide();
    $("#humidity_map_4").hide();

     $("#temperature_map_s").show();
    $("#humidity_map_s").show();
    $("#temperature_map_2_s").show();
    $("#humidity_map_2_s").show();
    $("#temperature_map_3_s").show();
    $("#humidity_map_3_s").show();
    $("#temperature_map_4_s").show();
    $("#humidity_map_4_s").show();
    $("#config_page").hide();
});

$("#config_btn").click(function(){
    $("#temperature_map").hide();
    $("#humidity_map").hide();
    $("#temperature_map_2").hide();
    $("#humidity_map_2").hide();
    $("#temperature_map_3").hide();
    $("#humidity_map_3").hide();
    $("#temperature_map_4").hide();
    $("#humidity_map_4").hide();

     $("#temperature_map_s").hide();
    $("#humidity_map_s").hide();
    $("#temperature_map_2_s").hide();
    $("#humidity_map_2_s").hide();
    $("#temperature_map_3_s").hide();
    $("#humidity_map_3_s").hide();
    $("#temperature_map_4_s").hide();
    $("#humidity_map_4_s").hide();
    $("#config_page").show();
});




var tooltip = d3.select("body")
                    .append("div")
                    .attr("class","hud")
                    .style("opacity",0);


                    
    

var grid_options = {
    fill:"#4286f4",
    fill_active:"#000",
    stroke:10,
    stroke_color:"#000",
    color:"#000",
    rx:10,
    ry:10,
    opacity:0.5,
    interactive:0,    
}

var device_options = {
    fill:"#fc6d5a",
    fill_active:"#f6c5f7",
    stroke:0,
    stroke_color:"#000",
    color:"#000",
    rx:0,
    ry:0,
    opacity:1,
    interactive:1    
}



var data =  {
    'one':{id:"Ddd",x:2,y:0,z:1,temperature:20,humidity:45},
    'two':{id:"Ddd",x:5,y:1,z:1,temperature:25,humidity:45},
    'three':{id:"Ddd",x:7,y:0,z:1,temperature:20,humidity:45},
    'four':{id:"Ddd",x:11,y:1,z:1,temperature:20,humidity:45},
    'five':{id:"Ddd",x:12,y:0,z:1,temperature:20,humidity:45},
    'six':{id:"Ddd",x:14,y:2,z:1,temperature:20,humidity:45},
    'seven':{id:"Ddd",x:21,y:2,z:1,temperature:20,humidity:45}

}

var cols_options = {
    fill:"#d4d6d8",
    fill_active:"#000",
    stroke:1,
    stroke_color:"#edeeef",
    color:"#000",
    rx:4,
    ry:4,
    opacity:1,
    interactive:0,    
}

var alleys_options = {
    fill:"#bfddff",
    fill_active:"#000",
    stroke:0,
    stroke_color:"#000",
    color:"#000",
    rx:3,
    ry:3,
    opacity:1,
    interactive:0,    
}

var text_array_1 = [
    {text:"LEVEL 1",x:330,y:50,size:'15px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'12px',rotation:0},
    {text:"X AXIS",x:0,y:130,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'14px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'14px',rotation:0},
    {text:"1",x:30,y:150,size:'12px',rotation:0},
    {text:"2",x:30,y:180,size:'12px',rotation:0},
    {text:"3",x:30,y:210,size:'12px',rotation:0},
    {text:"4",x:30,y:240,size:'12px',rotation:0},
    {text:"5",x:30,y:270,size:'12px',rotation:0},
    {text:"6",x:30,y:300,size:'12px',rotation:0},
    {text:"7",x:30,y:330,size:'12px',rotation:0},
    {text:"8",x:30,y:360,size:'12px',rotation:0},
    {text:"9",x:30,y:390,size:'12px',rotation:0},
    {text:"10",x:30,y:420,size:'12px',rotation:0},
    {text:"11",x:30,y:450,size:'12px',rotation:0},
    {text:"12",x:30,y:480,size:'12px',rotation:0},
    {text:"13",x:30,y:510,size:'12px',rotation:0},
    {text:"14",x:30,y:540,size:'12px',rotation:0},
    {text:"15",x:30,y:570,size:'12px',rotation:0},
    {text:"16",x:30,y:600,size:'12px',rotation:0},
    {text:"17",x:30,y:630,size:'12px',rotation:0},
    {text:"18",x:30,y:660,size:'12px',rotation:0},
    {text:"19",x:30,y:690,size:'12px',rotation:0},
    {text:"20",x:30,y:720,size:'12px',rotation:0},
    {text:"21",x:30,y:750,size:'12px',rotation:0},
    {text:"22",x:30,y:780,size:'12px',rotation:0},
    {text:"23",x:30,y:810,size:'12px',rotation:0},
    {text:"24",x:30,y:840,size:'12px',rotation:0},
    {text:"25",x:30,y:870,size:'12px',rotation:0},
    {text:"26",x:30,y:900,size:'12px',rotation:0},
    {text:"27",x:30,y:930,size:'12px',rotation:0},
    {text:"28",x:30,y:960,size:'12px',rotation:0},
    {text:"29",x:30,y:990,size:'12px',rotation:0},
    {text:"30",x:30,y:1020,size:'12px',rotation:0},
];

var text_array_2 = [
    {text:"LEVEL 2",x:330,y:50,size:'15px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'12px',rotation:0},
    {text:"X AXIS",x:0,y:130,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'14px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'14px',rotation:0},
    {text:"1",x:30,y:150,size:'12px',rotation:0},
    {text:"2",x:30,y:180,size:'12px',rotation:0},
    {text:"3",x:30,y:210,size:'12px',rotation:0},
    {text:"4",x:30,y:240,size:'12px',rotation:0},
    {text:"5",x:30,y:270,size:'12px',rotation:0},
    {text:"6",x:30,y:300,size:'12px',rotation:0},
    {text:"7",x:30,y:330,size:'12px',rotation:0},
    {text:"8",x:30,y:360,size:'12px',rotation:0},
    {text:"9",x:30,y:390,size:'12px',rotation:0},
    {text:"10",x:30,y:420,size:'12px',rotation:0},
    {text:"11",x:30,y:450,size:'12px',rotation:0},
    {text:"12",x:30,y:480,size:'12px',rotation:0},
    {text:"13",x:30,y:510,size:'12px',rotation:0},
    {text:"14",x:30,y:540,size:'12px',rotation:0},
    {text:"15",x:30,y:570,size:'12px',rotation:0},
    {text:"16",x:30,y:600,size:'12px',rotation:0},
    {text:"17",x:30,y:630,size:'12px',rotation:0},
    {text:"18",x:30,y:660,size:'12px',rotation:0},
    {text:"19",x:30,y:690,size:'12px',rotation:0},
    {text:"20",x:30,y:720,size:'12px',rotation:0},
    {text:"21",x:30,y:750,size:'12px',rotation:0},
    {text:"22",x:30,y:780,size:'12px',rotation:0},
    {text:"23",x:30,y:810,size:'12px',rotation:0},
    {text:"24",x:30,y:840,size:'12px',rotation:0},
    {text:"25",x:30,y:870,size:'12px',rotation:0},
    {text:"26",x:30,y:900,size:'12px',rotation:0},
    {text:"27",x:30,y:930,size:'12px',rotation:0},
    {text:"28",x:30,y:960,size:'12px',rotation:0},
    {text:"29",x:30,y:990,size:'12px',rotation:0},
    {text:"30",x:30,y:1020,size:'12px',rotation:0},
];

var text_array_3 = [
    {text:"LEVEL 3",x:330,y:50,size:'15px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'12px',rotation:0},
    {text:"X AXIS",x:0,y:130,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'14px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'14px',rotation:0},
    {text:"1",x:30,y:150,size:'12px',rotation:0},
    {text:"2",x:30,y:180,size:'12px',rotation:0},
    {text:"3",x:30,y:210,size:'12px',rotation:0},
    {text:"4",x:30,y:240,size:'12px',rotation:0},
    {text:"5",x:30,y:270,size:'12px',rotation:0},
    {text:"6",x:30,y:300,size:'12px',rotation:0},
    {text:"7",x:30,y:330,size:'12px',rotation:0},
    {text:"8",x:30,y:360,size:'12px',rotation:0},
    {text:"9",x:30,y:390,size:'12px',rotation:0},
    {text:"10",x:30,y:420,size:'12px',rotation:0},
    {text:"11",x:30,y:450,size:'12px',rotation:0},
    {text:"12",x:30,y:480,size:'12px',rotation:0},
    {text:"13",x:30,y:510,size:'12px',rotation:0},
    {text:"14",x:30,y:540,size:'12px',rotation:0},
    {text:"15",x:30,y:570,size:'12px',rotation:0},
    {text:"16",x:30,y:600,size:'12px',rotation:0},
    {text:"17",x:30,y:630,size:'12px',rotation:0},
    {text:"18",x:30,y:660,size:'12px',rotation:0},
    {text:"19",x:30,y:690,size:'12px',rotation:0},
    {text:"20",x:30,y:720,size:'12px',rotation:0},
    {text:"21",x:30,y:750,size:'12px',rotation:0},
    {text:"22",x:30,y:780,size:'12px',rotation:0},
    {text:"23",x:30,y:810,size:'12px',rotation:0},
    {text:"24",x:30,y:840,size:'12px',rotation:0},
    {text:"25",x:30,y:870,size:'12px',rotation:0},
    {text:"26",x:30,y:900,size:'12px',rotation:0},
    {text:"27",x:30,y:930,size:'12px',rotation:0},
    {text:"28",x:30,y:960,size:'12px',rotation:0},
    {text:"29",x:30,y:990,size:'12px',rotation:0},
    {text:"30",x:30,y:1020,size:'12px',rotation:0},
];

var text_array_4 = [
    {text:"LEVEL 4",x:330,y:50,size:'15px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'12px',rotation:0},
    {text:"X AXIS",x:0,y:130,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'14px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'14px',rotation:0},
    {text:"1",x:30,y:150,size:'12px',rotation:0},
    {text:"2",x:30,y:180,size:'12px',rotation:0},
    {text:"3",x:30,y:210,size:'12px',rotation:0},
    {text:"4",x:30,y:240,size:'12px',rotation:0},
    {text:"5",x:30,y:270,size:'12px',rotation:0},
    {text:"6",x:30,y:300,size:'12px',rotation:0},
    {text:"7",x:30,y:330,size:'12px',rotation:0},
    {text:"8",x:30,y:360,size:'12px',rotation:0},
    {text:"9",x:30,y:390,size:'12px',rotation:0},
    {text:"10",x:30,y:420,size:'12px',rotation:0},
    {text:"11",x:30,y:450,size:'12px',rotation:0},
    {text:"12",x:30,y:480,size:'12px',rotation:0},
    {text:"13",x:30,y:510,size:'12px',rotation:0},
    {text:"14",x:30,y:540,size:'12px',rotation:0},
    {text:"15",x:30,y:570,size:'12px',rotation:0},
    {text:"16",x:30,y:600,size:'12px',rotation:0},
    {text:"17",x:30,y:630,size:'12px',rotation:0},
    {text:"18",x:30,y:660,size:'12px',rotation:0},
    {text:"19",x:30,y:690,size:'12px',rotation:0},
    {text:"20",x:30,y:720,size:'12px',rotation:0},
    {text:"21",x:30,y:750,size:'12px',rotation:0},
    {text:"22",x:30,y:780,size:'12px',rotation:0},
    {text:"23",x:30,y:810,size:'12px',rotation:0},
    {text:"24",x:30,y:840,size:'12px',rotation:0},
    {text:"25",x:30,y:870,size:'12px',rotation:0},
    {text:"26",x:30,y:900,size:'12px',rotation:0},
    {text:"27",x:30,y:930,size:'12px',rotation:0},
    {text:"28",x:30,y:960,size:'12px',rotation:0},
    {text:"29",x:30,y:990,size:'12px',rotation:0},
    {text:"30",x:30,y:1020,size:'12px',rotation:0},
];

var text_array_1_s = [
     {text:"LEVEL 1",x:330,y:50,size:'10px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'10px',rotation:0},
    {text:"X AXIS",x:0,y:100,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'10px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'10px',rotation:0},
    {text:"1",x:30,y:150,size:'10px',rotation:0},
    {text:"2",x:30,y:180,size:'10px',rotation:0},
    {text:"3",x:30,y:210,size:'10px',rotation:0},
    {text:"4",x:30,y:240,size:'10px',rotation:0},
    {text:"5",x:30,y:270,size:'10px',rotation:0},
    {text:"6",x:30,y:300,size:'10px',rotation:0},
    {text:"7",x:30,y:330,size:'10px',rotation:0},
    {text:"8",x:30,y:360,size:'10px',rotation:0},
    {text:"9",x:30,y:390,size:'10px',rotation:0},
    {text:"10",x:20,y:420,size:'10px',rotation:0},
    {text:"11",x:20,y:450,size:'10px',rotation:0},
    {text:"12",x:20,y:480,size:'10px',rotation:0},
    {text:"13",x:20,y:510,size:'10px',rotation:0},
    {text:"14",x:20,y:540,size:'10px',rotation:0},
    {text:"15",x:20,y:570,size:'10px',rotation:0},
    {text:"16",x:20,y:600,size:'10px',rotation:0},
    {text:"17",x:20,y:630,size:'10px',rotation:0},
    {text:"18",x:20,y:660,size:'10px',rotation:0},
    {text:"19",x:20,y:690,size:'10px',rotation:0},
    {text:"20",x:20,y:720,size:'10px',rotation:0},
    {text:"21",x:20,y:750,size:'10px',rotation:0},
    {text:"22",x:20,y:780,size:'10px',rotation:0},
    {text:"23",x:20,y:810,size:'10px',rotation:0},
    {text:"24",x:20,y:840,size:'10px',rotation:0},
    {text:"25",x:20,y:870,size:'10px',rotation:0},
    {text:"26",x:20,y:900,size:'10px',rotation:0},
    {text:"27",x:20,y:930,size:'10px',rotation:0},
    {text:"28",x:20,y:960,size:'10px',rotation:0},
    {text:"29",x:20,y:990,size:'10px',rotation:0},
    {text:"30",x:20,y:1020,size:'10px',rotation:0}
];

var text_array_2_s = [
     {text:"LEVEL 2",x:330,y:50,size:'10px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'10px',rotation:0},
    {text:"X AXIS",x:0,y:100,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'10px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'10px',rotation:0},
    {text:"1",x:30,y:150,size:'10px',rotation:0},
    {text:"2",x:30,y:180,size:'10px',rotation:0},
    {text:"3",x:30,y:210,size:'10px',rotation:0},
    {text:"4",x:30,y:240,size:'10px',rotation:0},
    {text:"5",x:30,y:270,size:'10px',rotation:0},
    {text:"6",x:30,y:300,size:'10px',rotation:0},
    {text:"7",x:30,y:330,size:'10px',rotation:0},
    {text:"8",x:30,y:360,size:'10px',rotation:0},
    {text:"9",x:30,y:390,size:'10px',rotation:0},
    {text:"10",x:20,y:420,size:'10px',rotation:0},
    {text:"11",x:20,y:450,size:'10px',rotation:0},
    {text:"12",x:20,y:480,size:'10px',rotation:0},
    {text:"13",x:20,y:510,size:'10px',rotation:0},
    {text:"14",x:20,y:540,size:'10px',rotation:0},
    {text:"15",x:20,y:570,size:'10px',rotation:0},
    {text:"16",x:20,y:600,size:'10px',rotation:0},
    {text:"17",x:20,y:630,size:'10px',rotation:0},
    {text:"18",x:20,y:660,size:'10px',rotation:0},
    {text:"19",x:20,y:690,size:'10px',rotation:0},
    {text:"20",x:20,y:720,size:'10px',rotation:0},
    {text:"21",x:20,y:750,size:'10px',rotation:0},
    {text:"22",x:20,y:780,size:'10px',rotation:0},
    {text:"23",x:20,y:810,size:'10px',rotation:0},
    {text:"24",x:20,y:840,size:'10px',rotation:0},
    {text:"25",x:20,y:870,size:'10px',rotation:0},
    {text:"26",x:20,y:900,size:'10px',rotation:0},
    {text:"27",x:20,y:930,size:'10px',rotation:0},
    {text:"28",x:20,y:960,size:'10px',rotation:0},
    {text:"29",x:20,y:990,size:'10px',rotation:0},
    {text:"30",x:20,y:1020,size:'10px',rotation:0}
];

var text_array_3_s = [
     {text:"LEVEL 3",x:330,y:50,size:'10px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'10px',rotation:0},
    {text:"X AXIS",x:0,y:100,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'10px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'10px',rotation:0},
    {text:"1",x:30,y:150,size:'10px',rotation:0},
    {text:"2",x:30,y:180,size:'10px',rotation:0},
    {text:"3",x:30,y:210,size:'10px',rotation:0},
    {text:"4",x:30,y:240,size:'10px',rotation:0},
    {text:"5",x:30,y:270,size:'10px',rotation:0},
    {text:"6",x:30,y:300,size:'10px',rotation:0},
    {text:"7",x:30,y:330,size:'10px',rotation:0},
    {text:"8",x:30,y:360,size:'10px',rotation:0},
    {text:"9",x:30,y:390,size:'10px',rotation:0},
    {text:"10",x:20,y:420,size:'10px',rotation:0},
    {text:"11",x:20,y:450,size:'10px',rotation:0},
    {text:"12",x:20,y:480,size:'10px',rotation:0},
    {text:"13",x:20,y:510,size:'10px',rotation:0},
    {text:"14",x:20,y:540,size:'10px',rotation:0},
    {text:"15",x:20,y:570,size:'10px',rotation:0},
    {text:"16",x:20,y:600,size:'10px',rotation:0},
    {text:"17",x:20,y:630,size:'10px',rotation:0},
    {text:"18",x:20,y:660,size:'10px',rotation:0},
    {text:"19",x:20,y:690,size:'10px',rotation:0},
    {text:"20",x:20,y:720,size:'10px',rotation:0},
    {text:"21",x:20,y:750,size:'10px',rotation:0},
    {text:"22",x:20,y:780,size:'10px',rotation:0},
    {text:"23",x:20,y:810,size:'10px',rotation:0},
    {text:"24",x:20,y:840,size:'10px',rotation:0},
    {text:"25",x:20,y:870,size:'10px',rotation:0},
    {text:"26",x:20,y:900,size:'10px',rotation:0},
    {text:"27",x:20,y:930,size:'10px',rotation:0},
    {text:"28",x:20,y:960,size:'10px',rotation:0},
    {text:"29",x:20,y:990,size:'10px',rotation:0},
    {text:"30",x:20,y:1020,size:'10px',rotation:0}
];

var text_array_4_s = [
    {text:"LEVEL 4",x:330,y:50,size:'10px',rotation:0},
    {text:"Y AXIS",x:340,y:80,size:'10px',rotation:0},
    {text:"X AXIS",x:0,y:100,size:'10px',rotation:0},
    {text:"ALLEY 1",x:180,y:100,size:'10px',rotation:0},
    {text:"ALLEY 2",x:490,y:100,size:'10px',rotation:0},
    {text:"1",x:30,y:150,size:'10px',rotation:0},
    {text:"2",x:30,y:180,size:'10px',rotation:0},
    {text:"3",x:30,y:210,size:'10px',rotation:0},
    {text:"4",x:30,y:240,size:'10px',rotation:0},
    {text:"5",x:30,y:270,size:'10px',rotation:0},
    {text:"6",x:30,y:300,size:'10px',rotation:0},
    {text:"7",x:30,y:330,size:'10px',rotation:0},
    {text:"8",x:30,y:360,size:'10px',rotation:0},
    {text:"9",x:30,y:390,size:'10px',rotation:0},
    {text:"10",x:20,y:420,size:'10px',rotation:0},
    {text:"11",x:20,y:450,size:'10px',rotation:0},
    {text:"12",x:20,y:480,size:'10px',rotation:0},
    {text:"13",x:20,y:510,size:'10px',rotation:0},
    {text:"14",x:20,y:540,size:'10px',rotation:0},
    {text:"15",x:20,y:570,size:'10px',rotation:0},
    {text:"16",x:20,y:600,size:'10px',rotation:0},
    {text:"17",x:20,y:630,size:'10px',rotation:0},
    {text:"18",x:20,y:660,size:'10px',rotation:0},
    {text:"19",x:20,y:690,size:'10px',rotation:0},
    {text:"20",x:20,y:720,size:'10px',rotation:0},
    {text:"21",x:20,y:750,size:'10px',rotation:0},
    {text:"22",x:20,y:780,size:'10px',rotation:0},
    {text:"23",x:20,y:810,size:'10px',rotation:0},
    {text:"24",x:20,y:840,size:'10px',rotation:0},
    {text:"25",x:20,y:870,size:'10px',rotation:0},
    {text:"26",x:20,y:900,size:'10px',rotation:0},
    {text:"27",x:20,y:930,size:'10px',rotation:0},
    {text:"28",x:20,y:960,size:'10px',rotation:0},
    {text:"29",x:20,y:990,size:'10px',rotation:0},
    {text:"30",x:20,y:1020,size:'10px',rotation:0}
];



var url = "http://192.168.20.66:8081/get_data";

//generate_map(devices_1_1);
var empty = {};



    var columns_1 = generate_grid(1,30,50,130,30,30,cols_options);
    var columns_2 = generate_grid(1,30,360,130,30,30,cols_options);
    var columns_3 = generate_grid(1,30,670,130,30,30,cols_options);


    var alleys_1 = generate_grid(1,1,150,130,140,900,alleys_options);
    var alleys_2 = generate_grid(1,1,460,130,140,900,alleys_options);

    var border_1 = generate_grid(1,1,50,120,650,10,cols_options);
    var border_2 = generate_grid(1,1,50,1030,650,10,cols_options);

    var none_devs_1 = generate_grid(1,2,80,130,70,30,cols_options);
    var none_devs_2 = generate_grid(1,2,290,130,70,30,cols_options);
    var none_devs_3 = generate_grid(1,2,390,130,70,30,cols_options);
    var none_devs_4 = generate_grid(1,2,600,130,70,30,cols_options);

    var none_devs_1_s = generate_grid(1,1,80,130,70,30,cols_options);
    var none_devs_2_s = generate_grid(1,1,290,130,70,30,cols_options);
    var none_devs_3_s = generate_grid(1,1,390,130,70,30,cols_options);
    var none_devs_4_s = generate_grid(1,1,600,130,70,30,cols_options);

    var none_devs_5 = generate_grid(1,1,80,400,70,30,cols_options);
    var none_devs_6 = generate_grid(1,1,290,400,70,30,cols_options);
    var none_devs_7 = generate_grid(1,1,390,400,70,30,cols_options);
    var none_devs_8 = generate_grid(1,1,600,400,70,30,cols_options);

    var none_devs_9 = generate_grid(1,1,80,700,70,30,cols_options);
    var none_devs_10 = generate_grid(1,1,290,700,70,30,cols_options);
    var none_devs_11 = generate_grid(1,1,390,700,70,30,cols_options);
    var none_devs_12 = generate_grid(1,1,600,700,70,30,cols_options);

    var none_devs_13 = generate_grid(1,4,80,910,70,30,cols_options);
    var none_devs_14 = generate_grid(1,4,290,910,70,30,cols_options);
    var none_devs_15 = generate_grid(1,4,390,910,70,30,cols_options);
    var none_devs_16 = generate_grid(1,4,600,910,70,30,cols_options);

    var none_devs_13_s = generate_grid(1,3,80,940,70,30,cols_options);
    var none_devs_14_s = generate_grid(1,3,290,940,70,30,cols_options);
    var none_devs_15_s = generate_grid(1,3,390,940,70,30,cols_options);
    var none_devs_16_s = generate_grid(1,3,600,940,70,30,cols_options);

function make_map() {

       
    $.get(url,function(json){
        $("#not_respond").hide();
        $("#online").show();
        data = json;    
         var devices_1_1 = generate_devices(data,80,130,70,30,device_options,1,1,2);
    var devices_2_1 = generate_devices(data,220,130,70,30,device_options,2,1,2);
    var devices_3_1 = generate_devices(data,250,130,70,30,device_options,3,1,2);
    var devices_4_1 = generate_devices(data,390,130,70,30,device_options,4,1,2);

    var devices_hum_1_1 = generate_devices(data,80,130,70,30,device_options,1,1,1);
    var devices_hum_2_1 = generate_devices(data,220,130,70,30,device_options,2,1,1);
    var devices_hum_3_1 = generate_devices(data,250,130,70,30,device_options,3,1,1);
    var devices_hum_4_1 = generate_devices(data,390,130,70,30,device_options,4,1,1);

    var devices_1_2 = generate_devices(data,80,130,70,30,device_options,1,2,2);
    var devices_2_2 = generate_devices(data,220,130,70,30,device_options,2,2,2);
    var devices_3_2 = generate_devices(data,250,130,70,30,device_options,3,2,2);
    var devices_4_2 = generate_devices(data,390,130,70,30,device_options,4,2,2);

    var devices_hum_1_2 = generate_devices(data,80,130,70,30,device_options,1,2,1);
    var devices_hum_2_2 = generate_devices(data,220,130,70,30,device_options,2,2,1);
    var devices_hum_3_2 = generate_devices(data,250,130,70,30,device_options,3,2,1);
    var devices_hum_4_2 = generate_devices(data,390,130,70,30,device_options,4,2,1);
    
    var devices_1_3 = generate_devices(data,80,130,70,30,device_options,1,3,2);
    var devices_2_3 = generate_devices(data,220,130,70,30,device_options,2,3,2);
    var devices_3_3 = generate_devices(data,250,130,70,30,device_options,3,3,2);
    var devices_4_3 = generate_devices(data,390,130,70,30,device_options,4,3,2);

    var devices_hum_1_3 = generate_devices(data,80,130,70,30,device_options,1,3,1);
    var devices_hum_2_3 = generate_devices(data,220,130,70,30,device_options,2,3,1);
    var devices_hum_3_3 = generate_devices(data,250,130,70,30,device_options,3,3,1);
    var devices_hum_4_3 = generate_devices(data,390,130,70,30,device_options,4,3,1);

    var devices_1_4 = generate_devices(data,80,130,70,30,device_options,1,4,2);
    var devices_2_4 = generate_devices(data,220,130,70,30,device_options,2,4,2);
    var devices_3_4 = generate_devices(data,250,130,70,30,device_options,3,4,2);
    var devices_4_4 = generate_devices(data,390,130,70,30,device_options,4,4,2);

    var devices_hum_1_4 = generate_devices(data,80,130,70,30,device_options,1,4,1);
    var devices_hum_2_4 = generate_devices(data,220,130,70,30,device_options,2,4,1);
    var devices_hum_3_4 = generate_devices(data,250,130,70,30,device_options,3,4,1);
    var devices_hum_4_4 = generate_devices(data,390,130,70,30,device_options,4,4,1);

    var map_1 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_1_1)
                       .concat(devices_2_1).concat(devices_3_1).concat(devices_4_1).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
    var map_hum_1 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_hum_1_1)
                       .concat(devices_hum_2_1).concat(devices_hum_3_1).concat(devices_hum_4_1).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
     var map_2 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_1_2)
                       .concat(devices_2_2).concat(devices_3_2).concat(devices_4_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1_s).concat(none_devs_2_s).concat(none_devs_3_s).concat(none_devs_4_s)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13_s).concat(none_devs_14_s).concat(none_devs_15_s).concat(none_devs_16_s);
                       
    var map_hum_2 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_hum_1_2)
                       .concat(devices_hum_2_2).concat(devices_hum_3_2).concat(devices_hum_4_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1_s).concat(none_devs_2_s).concat(none_devs_3_s).concat(none_devs_4_s)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13_s).concat(none_devs_14_s).concat(none_devs_15_s).concat(none_devs_16_s);
     
     var map_3 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_1_3)
                       .concat(devices_2_3).concat(devices_3_3).concat(devices_4_3).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
    var map_hum_3 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_hum_1_3)
                       .concat(devices_hum_2_3).concat(devices_hum_3_3).concat(devices_hum_4_3).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);

    
     var map_4 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_1_4)
                       .concat(devices_2_4).concat(devices_3_4).concat(devices_4_4).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
    var map_hum_4 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(devices_hum_1_4)
                       .concat(devices_hum_2_4).concat(devices_hum_3_4).concat(devices_hum_4_4).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
                        d3.selectAll("svg").remove();
                        generate_map(map_1,text_array_1,"#temperature_map",0.7,500,750);
                        generate_map(map_hum_1,text_array_1,"#humidity_map",0.7,500,750);
                        
                        generate_map(map_2,text_array_2,"#temperature_map_2",0.7,500,750);
                        generate_map(map_hum_2,text_array_2,"#humidity_map_2",0.7,500,750);

                        generate_map(map_3,text_array_3,"#temperature_map_3",0.7,500,750);
                        generate_map(map_hum_3,text_array_3,"#humidity_map_3",0.7,500,750);

                        generate_map(map_4,text_array_4,"#temperature_map_4",0.7,500,750);
                        generate_map(map_hum_4,text_array_4,"#humidity_map_4",0.7,500,750);

                        generate_map(map_1,text_array_1_s,"#temperature_map_s",0.4,300,420);
                        generate_map(map_hum_1,text_array_1_s,"#humidity_map_s",0.4,300,420);
                        

                        generate_map(map_2,text_array_2_s,"#temperature_map_2_s",0.4,300,420);
                        generate_map(map_hum_2,text_array_2_s,"#humidity_map_2_s",0.4,300,420);

                        generate_map(map_3,text_array_3_s,"#temperature_map_3_s",0.4,300,420);
                        generate_map(map_hum_3,text_array_3_s,"#humidity_map_3_s",0.4,300,420);

                        generate_map(map_4,text_array_4_s,"#temperature_map_4_s",0.4,300,420);
                        generate_map(map_hum_4,text_array_4_s,"#humidity_map_4_s",0.4,300,420);    
    }).fail(function(){
         $("#not_respond").show();
         $("#online").hide();
         var map_1 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
    var map_hum_1 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
     var map_2 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1_s).concat(none_devs_2_s).concat(none_devs_3_s).concat(none_devs_4_s)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13_s).concat(none_devs_14_s).concat(none_devs_15_s).concat(none_devs_16_s);
                       
    var map_hum_2 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1_s).concat(none_devs_2_s).concat(none_devs_3_s).concat(none_devs_4_s)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13_s).concat(none_devs_14_s).concat(none_devs_15_s).concat(none_devs_16_s);
     
     var map_3 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
    var map_hum_3 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);

    
     var map_4 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
    
    var map_hum_4 = columns_1.concat(columns_2).concat(columns_3).concat(alleys_1).concat(alleys_2).concat(border_1).concat(border_2)
                       .concat(none_devs_1).concat(none_devs_2).concat(none_devs_3).concat(none_devs_4)
                       .concat(none_devs_5).concat(none_devs_6).concat(none_devs_7).concat(none_devs_8)
                       .concat(none_devs_9).concat(none_devs_10).concat(none_devs_11).concat(none_devs_12)
                       .concat(none_devs_13).concat(none_devs_14).concat(none_devs_15).concat(none_devs_16);
                        d3.selectAll("svg").remove();
                        generate_map(map_1,text_array_1,"#temperature_map",0.7,500,750);
                        generate_map(map_hum_1,text_array_1,"#humidity_map",0.7,500,750);
                        
                        generate_map(map_2,text_array_2,"#temperature_map_2",0.7,500,750);
                        generate_map(map_hum_2,text_array_2,"#humidity_map_2",0.7,500,750);

                        generate_map(map_3,text_array_3,"#temperature_map_3",0.7,500,750);
                        generate_map(map_hum_3,text_array_3,"#humidity_map_3",0.7,500,750);

                        generate_map(map_4,text_array_4,"#temperature_map_4",0.7,500,750);
                        generate_map(map_hum_4,text_array_4,"#humidity_map_4",0.7,500,750);

                        generate_map(map_1,text_array_1_s,"#temperature_map_s",0.4,300,420);
                        generate_map(map_hum_1,text_array_1_s,"#humidity_map_s",0.4,300,420);
                        

                        generate_map(map_2,text_array_2_s,"#temperature_map_2_s",0.4,300,420);
                        generate_map(map_hum_2,text_array_2_s,"#humidity_map_2_s",0.4,300,420);

                        generate_map(map_3,text_array_3_s,"#temperature_map_3_s",0.4,300,420);
                        generate_map(map_hum_3,text_array_3_s,"#humidity_map_3_s",0.4,300,420);

                        generate_map(map_4,text_array_4_s,"#temperature_map_4_s",0.4,300,420);
                        generate_map(map_hum_4,text_array_4_s,"#humidity_map_4_s",0.4,300,420); 
    });
}
    




make_map();
make_map();
setInterval(make_map, 5000);