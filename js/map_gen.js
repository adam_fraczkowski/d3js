



function convertIso(x, y) { 
    // m = 1 / sqrt(2);
    var m = Math.sin(Math.PI/4);
    // rotated would be (x*m - y*m, x*m+y*m),
    // but we divide y by m to get the foreshortening
    return { x : x * m - y * m, y : x + y };
}



function map(x,in_min,in_max,out_min,out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}




function convert_temp_color(temp)
{
    var h_factor = map(temp,scale_object.heat_scale_min,scale_object.heat_scale_max,220,0);
    return [Math.round(h_factor),100,50];
}

function convert_hum_color(humidity)
{
    var l_factor = map(humidity,scale_object.hum_scale_min,scale_object.hum_scale_max,100,20);
    return [248,100,Math.round(l_factor)];
}




function generate_grid(rows,cols,start_x,start_y,square_w,square_h,options)
{
    var map = [];
    for(var i=0;i<rows;i++)
    {
        for(var j=0;j<cols;j++)
        {
            map.push({
                    x:i*square_w+start_x,
                    y:j*square_h+start_y,
                    width:square_w,
                    height:square_h,
                    color:options.color,
                    fill:options.fill,
                    fill_active:options.fill_active,
                    stroke:options.stroke,
                    stroke_color:options.stroke_color,
                    rx:options.rx,
                    ry:options.ry,
                    opacity:options.opacity,
                    interactive:options.interactive,
                    text:options.text
                    })
        }
    }
    return map;
}
//type: humidity:1 temperature:2
function generate_devices(data,start_x,start_y,square_w,square_h,options,column,level,type)
{
    var map = [];
    var custom_fill='';
    for (var key in data) {
        if(data.hasOwnProperty(key) && data[key].y==column && data[key].z==level) 
        {
            switch(type)
            {
                case 1:
                color_hsl = convert_hum_color(parseFloat(data[key].humidity));
                custom_fill =  "hsl("+color_hsl[0]+","+color_hsl[1]+"%,"+color_hsl[2]+"%)";
                //console.log(parseFloat(custom_fill));
                break;

                case 2:
                //console.log(parseFloat(data[key].temperature));
                color_hsl = convert_temp_color(parseFloat(data[key].temperature));
                //console.log(color_hsl);
                custom_fill =  "hsl("+color_hsl[0]+","+color_hsl[1]+"%,"+color_hsl[2]+"%)";
                //console.log(custom_fill);
                //console.log(custom_fill);
                break;

                default:
                custom_fill=options.fill;
                break;

            }

            map.push({
                    x:(data[key].y-1)*square_w+start_x,
                    y:(data[key].x-1)*square_h+start_y,
                    width:square_w,
                    height:square_h,
                    color:options.color,
                    fill:custom_fill,
                    fill_active:options.fill_active,
                    stroke:options.stroke,
                    stroke_color:options.stroke_color,
                    rx:options.rx,
                    ry:options.ry,
                    opacity:options.opacity,
                    interactive:options.interactive,
                    text:"ID:"+data[key].id+"<br>Temperature:"+data[key].temperature+""+"<br>Humidity:"
                    +data[key].humidity+""+"<br>X:"+data[key].x+"<br>Y:"+data[key].y+"<br>Z:"+data[key].z+"<br>"
                })
        }
    }
    return map;
}

function generate_map(map_array,text_array,html_id,scale,width,height)
{
    var canvas = d3.select(html_id).append("svg").attr("width",width).attr("height",height).attr("transform", "translate(0)scale(1)")
                                  
    var text_layer = canvas.selectAll(".texts")
                           .data(text_array)
                           .enter()
                           .append("text")
                           .attr("width",100*scale)
                           .attr("height",100*scale)
                           .attr("x",function(data){return data.x*scale})
                           .attr("y",function(data){return data.y*scale})
                           .text(function(data){return data.text})
                           .attr("class","texts")
                           .attr("font-size",function(data){return data.size})
                           .attr("font-family","Arial")
                           .attr("fill","black")
                           .attr("transform",function(data){return "rotate("+data.rotation+")"});

    
    
    var squares = canvas.selectAll(".squares")
                        .data(map_array)
                        .enter()
                        .append("rect")
                        .attr("width",function(square){return square.width*scale})
                        .attr("height",function(square){return square.height*scale})
                        .attr("class","squares")
                        .attr("x",function(square){return square.x*scale})
                        .attr("y",function(square){return square.y*scale})
                        .attr("fill",function(square){return square.fill})
                        .attr("stroke-width",function(square){return square.stroke})
                        .attr("stroke",function(square){return square.stroke_color})
                        .attr("color",function(square){return square.color})
                        .attr("rx",function(square){return square.rx})
                        .attr("ry",function(square){return square.ry})
                        .attr("opacity",function(square){return square.opacity})
                        .on("mouseover", function(square) {		
                            if(square.interactive==1)
                            {
                                d3.select(this).attr("fill",square.fill_active);
                                tooltip.transition()		
                                    .duration(200)		
                                    .style("opacity", 1)
                                    .style("left", (d3.event.pageX - 54) + "px")
                                    .style("top", (d3.event.pageY + 20) + "px");
                                            
                                tooltip.html(square.text);	
                            
                            }
                            })					
                        .on("mouseout", function(square) {		
                            if(square.interactive==1)
                            {   
                                d3.select(this).attr("fill",square.fill);
                                tooltip.transition()		
                                    .duration(500)		
                                    .style("opacity", 0);	
                            }
                        });

}

function convertIso( x, y ) { 
    var m = 1 / Math.sqrt(2);
    //var m = Math.sin(Math.PI/4);
    // rotated would be (x*m - y*m, x*m+y*m),
    // but we divide y by m to get the foreshortening
    return { x : x * m - y * m, y : x + y };
}

function generate_map_iso(map_array,text_array)
{
    var canvas = d3.select("body").append("svg").attr("width",1000).attr("height",1000).attr("transform", "translate(500,0)scale(0.5)")
                                  
    var squares = canvas.selectAll(".squares")
                        .data(map_array)
                        .enter()
                        .append("polyline")
                        .attr("class","squares")
                        .attr("points",function(square){
                            var x1 = square.x;
                            var y1 = square.y;
                            var x2 = square.x+square.width;
                            var y2 = y1;
                            var x3 = x2;
                            var y3 = square.y+square.height
                            var x4 = square.x;
                            var y4 = square.y+square.height;
                            
                            var point_1 = convertIso(x1,y1);
                            var point_2 = convertIso(x2,y2);
                            var point_3 = convertIso(x3,y3);
                            var point_4 = convertIso(x4,y4);
                            
                            points = point_1.x+","+point_1.y+" "+point_2.x+","+point_2.y+" "+point_3.x+","+point_3.y+" "+point_4.x+","+point_4.y;
                            return points;
                        })
                        .attr("fill",function(square){return square.fill})
                        .attr("stroke-width",function(square){return square.stroke})
                        .attr("stroke",function(square){return square.stroke_color})
                        .attr("color",function(square){return square.color})
                        .attr("rx",function(square){return square.rx})
                        .attr("ry",function(square){return square.ry})
                        .attr("opacity",function(square){return square.opacity})
                        .on("mouseover", function(square) {		
                            if(square.interactive==1)
                            {
                                d3.select(this).attr("fill",square.fill_active);
                                tooltip.transition()		
                                    .duration(200)		
                                    .style("opacity", .5);
                                            
                                tooltip.text(square.text);	
                            
                            }
                            })					
                        .on("mouseout", function(square) {		
                            if(square.interactive==1)
                            {   
                                d3.select(this).attr("fill",square.fill);
                                tooltip.transition()		
                                    .duration(500)		
                                    .style("opacity", 0);	
                            }
                        });

}
